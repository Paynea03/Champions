# Champions
COM3504 Intelligent Web - Assignment 2020-2021<br/>
A secret messaging app to ensure secret operations

## Team Members
|   | Name         | Email                   | Main Contributions                                       |
|---|--------------|-------------------------|----------------------------------------------------------|
| 1 | Andrew Payne | apayne5@sheffield.ac.uk | socket.io, service worker, the chat/annotation interface |
| 2 | Zeyi Liu     | zliu62@sheffield.ac.uk  | nodeJS server (excluding socket.io), MongoDB             |
| 3 | Zhihui Lin   | zlin18@sheffield.ac.uk  | IndexedDb, Ajax communication, Swagger documentation     |

**PS**: The Knowledge Graph part is completed by Andrew Payne and Zhihui Lin.
## Installation
1. Clone the repository via HTTPS/SSH

    ```
    git clone https://github.com/zl1n/Champions.git
    ```
   or
    ```
    git clone git@github.com:zl1n/Champions.git
    ```
2. Start your mongoDB client on port `27017`
3. Go to main node js server
    ```
    cd Champions/solution/main_server
    ```

4. Install all required dependencies specified in package.json
    ```
    npm install
    ```

5. Start the main node js server
    ```
    npm start
    ```
6. Go to mongoDB server
    ```
    cd Champions/solution/mongo_server
    ```

7. Install all required dependencies specified in package.json
    ```
    npm install
    ```

8. Start the mongoDB server
    ```
    npm start
    ```
9. You are good to go

## Swagger/OpenAPI Specification
The Swagger/OpenAPI specification is available at<br/> <http://127.0.0.1:3000/api-docs/>
