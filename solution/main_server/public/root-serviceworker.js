// service worker scope: entire project

/**
 * The following code is taken and adapted from COM3504
 * Lectures and LabClasses - Wk3
 */
//cache setup
let cache_name = "champions-cache-v1";
//urls to use inside the service worker
let urlsToCache = [
  "/",
  "/stylesheets/style.css",
  "/javascripts/main.js",
  "/javascripts/canvas.js",
  "/javascripts/index.js",
  "/javascripts/idb/idb.js",
  "/javascripts/idb/database.js",
  "/javascripts/idb/annotation.js",
  "/javascripts/idb/image.js",
  "/javascripts/idb/chatlog.js",
  "manifest.json",
  "/favico/favicon-32x32.png",
  "/favico/favicon-16x16.png",
  "/favico/android-chrome-192x192.png",
  "/socket.io/socket.io.js",
];

/**
 * Cache Install. called on page load and install service worker
 * */
self.addEventListener("install", (evt) => {
  evt.waitUntil(
    caches
      .open(cache_name)
      .then((cache) => {
        console.log("Opened cache");
        //add all urls that will be used by the service worker.
        return cache.addAll(urlsToCache);
      })
      .catch((err) => {
        console.log("install error: " + err);
      })
  );
});

// cache activation - form Week 4 lab class
self.addEventListener("activate", function (e) {
  console.log("[ServiceWorker] Activate");
  e.waitUntil(
    caches.keys().then(function (keyList) {
      return Promise.all(
        keyList.map(function (key) {
          if (key !== cache_name) {
            console.log("[ServiceWorker] Removing old cache", key);
            return caches.delete(key);
          }
        })
      );
    })
  );
  return self.clients.claim();
});

/**
 * Cache Fetch page handler
 * @evt is the event occurance
 * */

self.addEventListener("fetch", (evt) => {
  evt.respondWith(
    caches
      .match(evt.request)
      .then((response) => {
        if (response) {
          return response;
        }
        let fetchRequest = evt.request.clone();
        return fetch(fetchRequest).then((response) => {
          if (
            !response ||
            response.status !== 200 ||
            response.type !== "basic"
          ) {
            return response;
          }

          let responseToCache = response.clone();
          caches
            .open(cache_name)
            .then((cache) => {
              cache.put(evt.request, responseToCache);
            })
            .catch((err) => {
              console.log("Error: " + err);
            });
          return response;
        });
      })
      .catch((err) => {})
  );
});
