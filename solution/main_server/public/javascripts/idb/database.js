/**
 * the functions accessing the indexedDB
 *
 * @author Zhihui Lin
 */

'use strict'
let db_local_persistence;
const DB_NAME = 'db_local_persistence';
const DB_VERSION = 1;

/**
 * init the IndexedDB with multiple object stores
 *
 * @author Zhihui Lin
 */
async function initDatabase() {
  if (!db_local_persistence) {
    db_local_persistence = await idb.openDB(DB_NAME, DB_VERSION, {
      upgrade(upgradeDb, oldVersion, newVersion, transaction) {
        initImageStore(upgradeDb);
        initAnnotationStore(upgradeDb);
        initChatLogStore(upgradeDb);
      }
    });
    await pruneDatabase();
  }
}

/**
 * housekeeping utility that cleans up irrelevant databases,
 * calls to keep one database per app.
 *
 * @author Zhihui Lin
 */
async function pruneDatabase() {
  // get information of all existing databases as an array
  indexedDB.databases().then(dbs => dbs.forEach(db => {
    // if the name of an existing database is not equal to the name of the database we created for this app
    if (db.name !== DB_NAME) {
      idb.deleteDB(db.name);
      console.log('[IndexedDB] irrelevant database' + db.name + ' deleted');
    }
  }));
}
