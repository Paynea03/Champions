/**
 * Functions for the Object store 'annotation'
 *
 * @author Zhihui Lin
 */

'use strict'
const STORE_ANNOTATION = 'store_annotation';

/**
 * Initialise the Object store 'annotation',
 * please note that IndexedDB is schema-less thus ONLY PK (key path) and indexes will be defined here
 *
 * @author Zhihui Lin
 */
async function initAnnotationStore(upgradeDb) {
  if (!upgradeDb.objectStoreNames.contains(STORE_ANNOTATION)) {
    // Set the primary key to be automatically assigned as an auto-incrementing number
    let annotationStore = await upgradeDb.createObjectStore(STORE_ANNOTATION, {
      keyPath: 'id',
      autoIncrement: true
    });
    await annotationStore.createIndex('by_room', 'room', {unique: false, multiEntry: true});
    await annotationStore.createIndex('by_room_user', ['room', 'user'], {unique: false});
    console.log('[IndexedDB] Object store ' + STORE_ANNOTATION + ' created');
  }
}

/**
 * Store the annotation data with the corresponding room id and user id
 * @param roomID the room id
 * @param userID the message sender's id
 * @param canvasAnnot the annotation object contains col, height, thick, width, x1, x2, y1, y2
 *
 * @author Zhihui Lin
 */
async function storeAnnotation(roomID, userID, canvasAnnot) {
  if (db_local_persistence) {
    const tx = await db_local_persistence.transaction(STORE_ANNOTATION, "readwrite");
    // store room id, user id and annotation object to the local IndexedDB
    await tx.store.add({room: roomID, user: userID, canvasAnnot: canvasAnnot});
    await tx.done;
  }
}

/**
 * Load all annotations belongs to the current room (if they exist and
 * if the current user has been involved in this communication.)
 *
 * @param roomID the room id
 * @param userID the current user's id
 *
 * @author Zhihui Lin
 */
async function retrieveAnnotation(ctx, roomID, userID) {
  if (db_local_persistence) {
    const tx = await db_local_persistence.transaction([STORE_ANNOTATION, STORE_CHATLOG], "readonly");
    const annotationStore = await tx.objectStore(STORE_ANNOTATION);
    const chatLogStore = await tx.objectStore(STORE_CHATLOG);
    // get indexes
    const room_userIndex_anon = await annotationStore.index('by_room_user');
    const room_userIndex_chat = await chatLogStore.index('by_room_user');

    // to see if the current user has been involved in the communication
    let isAnonParticipant = await room_userIndex_anon.openCursor(IDBKeyRange.only([roomID, userID]));
    let isChatParticipant = await room_userIndex_chat.openCursor(IDBKeyRange.only([roomID, userID]));

    // IMPORTANT: stop if the user was not participated in neither the chat nor the annotation
    if (!isAnonParticipant && !isChatParticipant) return "[IndexedDB] No annotations found in the room " + roomID;

    // get all annotations related to the current room given the current user is a legitimate participant
    const roomIndex = await annotationStore.index('by_room');
    let annotations = await roomIndex.getAll(roomID);

    for (let i = 0, n = annotations.length; i < n; i++) {
      // get each annotation entry
      let anno = annotations[i];
      // render each annotation again
      ctx.beginPath();
      ctx.moveTo(anno.canvasAnnot.x1, anno.canvasAnnot.y1);
      ctx.lineTo(anno.canvasAnnot.x2, anno.canvasAnnot.y2);
      ctx.strokeStyle = anno.canvasAnnot.col;
      ctx.lineWidth = anno.canvasAnnot.thick;
      ctx.stroke();
      ctx.closePath();
    }
    return "[IndexedDB] All history annotations are loaded!";
  }
}

/**
 * Remove all the annotations related to this room from the local IndexedDB
 * @param roomID the room id
 *
 * @author Zhihui Lin
 */
async function clearAnnotation(roomID) {
  if (db_local_persistence) {
    const tx = await db_local_persistence.transaction(STORE_ANNOTATION, "readwrite");
    const annotationStore = await tx.objectStore(STORE_ANNOTATION);
    // get annotation related to the current room only
    let annotations = await annotationStore.index('by_room').getAll(roomID);

    if (annotations.length !== 0) {
      for (let i = 0, n = annotations.length; i < n; i++) {
        // remove all the annotations related to this room
        await annotationStore.delete(annotations[i].id);
      }
      await tx.done;
      return "[IndexedDB] Annotations cleared!";
    } else return "[IndexedDB] No annotations need to be cleared in the room " + roomID;
  }
}
