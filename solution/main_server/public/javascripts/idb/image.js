/**
 * Functions for the Object store 'image'
 *
 * @author Zhihui Lin
 */

'use strict'
const STORE_IMAGE = 'store_image';

/**
 * Initialise the Object store 'image',
 * please note that IndexedDB is schema-less thus ONLY PK (key path) and indexes will be defined here
 *
 * @author Zhihui Lin
 */
async function initImageStore(upgradeDb) {
  if (!upgradeDb.objectStoreNames.contains(STORE_IMAGE)) {
    // Set the primary key to be automatically assigned as an auto-incrementing number
    let imageStore = await upgradeDb.createObjectStore(STORE_IMAGE, {
      keyPath: 'url',
      autoIncrement: true
    });
    // Set an index on the 'author_name' property of the image
    await imageStore.createIndex('by_user', 'user', {unique: false, multiEntry: true});
    console.log('[IndexedDB] Object store ' + STORE_IMAGE + ' created');
  }
}

/**
 * Cache the image from a direct link provided by the user, it stores base64 encoding
 * of the image, user's name, the url/link and the time
 *
 * @param image base64 of the image
 * @param userID user/author's name
 * @param url the direct link/url
 * @returns {Promise<void>}
 * @author Zhihui Lin
 */
async function cacheRemoteImage(image, userID, src) {
  let image_base64_head = "data:image/";
  // if the src of the image is not from a remote url, stop caching
  if (!db_local_persistence || src.includes(image_base64_head) || src.includes(window.location.origin)) {
    return;
  }

  await retrieveCachedImage(src).then(r => {
    if (r === undefined || r.image === undefined) {
      // open transaction
      const tx = db_local_persistence.transaction(STORE_IMAGE, "readwrite");

      // cache the image
      tx.store.add({
        image: image,
        user: userID,
        cachedAt: new Date().toLocaleString("en-GB"),
        url: src
      });
      tx.done;
    }
  });
}

/**
 * Get the cached image from IndexedDB if exists
 *
 * @param url the direct link
 * @returns {Promise<*>}
 * @author Zhihui Lin
 */
async function retrieveCachedImage(url) {
  if (db_local_persistence) {
    // open transaction
    const tx = await db_local_persistence.transaction(STORE_IMAGE, "readonly");
    const imageStore = await tx.objectStore(STORE_IMAGE);
    // get the cached image by primary key (the direct link)
    return await imageStore.get(url);
  }
}
