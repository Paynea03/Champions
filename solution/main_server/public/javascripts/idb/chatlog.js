/**
 * Functions for the Object store 'chatlog'
 *
 * @author Zhihui Lin
 */

'use strict'
const STORE_CHATLOG = 'store_chatlog';

/**
 * Initialise the Object store 'chatlog',
 * please note that IndexedDB is schema-less thus ONLY PK (key path) and indexes will be defined here
 *
 * @author Zhihui Lin
 */
async function initChatLogStore(upgradeDb) {
  if (!upgradeDb.objectStoreNames.contains(STORE_CHATLOG)) {
    // Set the primary key to be automatically assigned as an auto-incrementing number
    let chatLogStore = await upgradeDb.createObjectStore(STORE_CHATLOG, {
      keyPath: 'id',
      autoIncrement: true
    });
    // Index the data by "room id" and "room id + user's name"
    await chatLogStore.createIndex('by_room', 'room', {unique: false, multiEntry: true});
    await chatLogStore.createIndex('by_room_user', ['room', 'user'], {unique: false});
    console.log('[IndexedDB] Object store ' + STORE_CHATLOG + ' created');
  }
}

/**
 * Store the all the chat histories: chat content, Json-LD of Knowledge Graph and transition message
 * with the corresponding room id and user id
 *
 * @param roomID the room id
 * @param userID the message sender's id
 * @param chatLog it could be a text message, a knowledge graph message or a transition notification
 * @param time when the message was sent
 * @param isTransition boolean value - true if it is a transition notification
 * @param isKnowledge boolean value - true if it is a knowledge graph result
 * @param kgColor current canvas color
 * @author Zhihui Lin
 */
async function storeChatLog(roomID, userID, chatLog, time, kgColor, isTransition, isKnowledge) {
  if (db_local_persistence) {
    const tx = await db_local_persistence.transaction(STORE_CHATLOG, "readwrite");
    // add the data to the object store of the local IndexedDB
    await tx.store.add({
      room: roomID,
      user: userID,
      message: chatLog,
      time: time,
      kgColor: kgColor,
      isTransition: isTransition,
      isKnowledge: isKnowledge
    });
    await tx.done;
  }
}

/**
 * Load all chat logs and transition message belongs to the current room (if they exist and
 * if the current user has been involved in this communication.)
 *
 * @param roomID the room id
 * @param userID the message sender's id
 * @author Zhihui Lin
 */
async function retrieveChatLog(roomID, userID) {
  if (db_local_persistence) {
    const tx = await db_local_persistence.transaction([STORE_CHATLOG, STORE_ANNOTATION], "readonly");
    const chatLogStore = await tx.objectStore(STORE_CHATLOG);
    const annotationStore = await tx.objectStore(STORE_ANNOTATION);
    // get indexes
    const room_userIndex_chat = await chatLogStore.index('by_room_user');
    const room_userIndex_anon = await annotationStore.index('by_room_user');

    // to see if the current user has been involved in this communication
    let isChatParticipant = await room_userIndex_chat.openCursor(IDBKeyRange.only([roomID, userID]));
    let isAnonParticipant = await room_userIndex_anon.openCursor(IDBKeyRange.only([roomID, userID]));

    // IMPORTANT: stop if the user was not participated in neither the chat nor the annotation
    if (!isChatParticipant && !isAnonParticipant) return "[IndexedDB] No messages found in the room " + roomID;

    // get all messages related to the current room given the current user is a legitimate participant
    const roomIndex = await chatLogStore.index('by_room');
    let chatLogs = await roomIndex.getAll(roomID);

    for (let i = 0, n = chatLogs.length; i < n; i++) {
      // get each chat content
      let chat = chatLogs[i];
      // render the chat history
      // if its a normal chat message and the current user is the sender
      if (userID === chat.user && chat.isTransition === false && chat.isKnowledge === false) {
        writeOnChatHistory("<div class='alert alert-success message-box-r' role='alert'>" +
          " <p>" + chat.time + "</p> <i class='fas fa-user-secret fa-lg'></i>" +
          " <b>Me:</b> " + chat.message + "</div>");
      }
      // if its a normal chat message but the current user is the receiver
      else if (chat.isTransition === false && chat.isKnowledge === false) {
        writeOnChatHistory("<div class='alert alert-secondary message-box-l' role='alert'>" +
          " <p>" + chat.time + "</p> <i class='fas fa-user-ninja fa-lg'></i>" +
          " <b>" + chat.user + ":</b> " + chat.message + "</div>");
      }
      // if its a transition message
      else if (chat.isTransition === true) {
        document.getElementById("history").insertAdjacentHTML('beforeend', chat.message);
      }
      else if (chat.isKnowledge === true) {
        writeKGResult(chat.message, chat.kgColor, chat.user)
      }
    }
    return "[IndexedDB] All history messages are loaded!";
  }
}
