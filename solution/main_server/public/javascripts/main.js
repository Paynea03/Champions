// load service worker (may change to be as part of another function
// as project progresses. currently loads with body onload in index.ejs)

/**
 * Load service worker called from body on load. registers service worker on open
 * */
const loadServiceWorker = () => {
  if ("serviceWorker" in navigator) {
    navigator.serviceWorker
      .register("/root-serviceworker.js", { scope: "/" })
      .then((registration) => {
        // if successful
        console.log(
          "ServiceWorker registration successful with scope: ",
          registration.scope
        );
      })
      .catch((error) => {
        // if load fails
        console.log("ServiceWorker registration failed: ", error);
      });
  } else {
    //print if browser not compatible or other issue with compatibility
    console.log("Service Worker NOT available");
  }
};

/**
 * Determine if the browser supports IndexedDB and initialize the IndexedDB if it does.
 * @author Zhihui Lin
 */
function initIndexedDB() {
  // check if the browser supports IndexedDB
  if ("indexedDB" in window) {
    // initialise the IndexedDB if browser is compatible
    initDatabase();
  } else {
    //print if browser not compatible or other issue with compatibility
    console.log(
      "The stable version of IndexedDB is not supported by your browser. Functions such as these will not be available."
    );
  }
}

/**
 * it sends an Ajax query using JQuery
 * @param url the url to send to
 * @param data the data to send (e.g. a Javascript structure)
 */
// let imageList = [];
function listImages(elem, url) {
  $.ajax({
    url: "/api/image",
    contentType: "application/json",
    dataType: "json",
    type: "GET",
    success: (dataR) => {
      // adapted from https://stackoverflow.com/questions/9895082/javascript-populate-drop-down-list-with-array
      let imageList = document.getElementById(elem);

      for (let i = 0; i < dataR.length; i++) {
        let image = dataR[i];
        let el = document.createElement("option");
        el.textContent = imageName(image);
        el.value = `images/${image}`;
        imageList.appendChild(el);
      }
    },
    error: function (response) {
      //image list is cached. in event unavailable, return warning
      alert(
        "No Network Connection. " +
          "Image list is unavailable. " +
          "Any images uploaded will be added to the server " +
          "once a network connection is established"
      );
    },
  });
}
