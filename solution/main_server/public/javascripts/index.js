let name = null;
let roomNo = null;
let savedRoom = null;

let socket = null;
let chat = null;

//check if online, socket.io not available if offline
if (window.navigator.onLine) {
  socket = io.connect("/draw");
  chat = io.connect("/chat");
}


let snapshotValidation = true;
let imageValidation = true;
let uploadImgBase64Data = null;

//when creating dynamic modals, add increment to ensure no duplicate ids
let dynamicModalCount = 1;

// variables for accessing the Knowledge Graph
const apiKey = "AIzaSyAG7w627q-djB4gTTahssufwNOImRqdYKM";
const service_url = "https://kgsearch.googleapis.com/v1/entities:search";

/**
 * called by <body onload>
 * it initialises the interface and the expected socket messages
 * plus the associated actions
 */
const init = () => {
  // it sets up the interface so that userId and room are selected
  document.getElementById("initial_form").style.display = "block";
  document.getElementById("chat_interface").style.display = "none";

  if (window.navigator.onLine) {
    initChatSocket();
  }
  loadServiceWorker();
  initIndexedDB();
  listImages("image_url");

  let offline = document.getElementById("homeOfflineAlert");

  if (window.navigator.onLine) {
    offline.style.display = "none";
  } else {
    offline.style.display = "block";
  }

  /**
   * following taken from https://www.w3schools.com/bootstrap4/bootstrap_forms_custom.asp
   * Add the following code if you want the name of the file appear on select
   */
  $(".custom-file-input").on("change", function () {
    let fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });
};

/**
 * called to generate a random room number
 * This is a simplification. A real world implementation would ask the server to generate a unique room number
 * so to make sure that the room number is not accidentally repeated across uses
 */
const generateRoom = () => {
  roomNo = Math.round(Math.random() * 10000);
  document.getElementById("roomNo").value = "R" + roomNo;
};

/**
 * the following code is for socket.io and has been reused and adapted from the COM3504 Lab solutions
 * initialises that chat sockets for sending and receiving chat
 */
const initChatSocket = () => {
  let currentTime = new Date().toLocaleString("en-GB");

  // called when someone joins the room. If it is someone else it notifies the joining of the room
  chat.on("joined", (room, userId) => {
    if (userId === name) {
      // it enters the chat
      hideLoginInterface(room, userId);
      // load all chat logs belongs to the current user and room (if they exist)
      retrieveChatLog(room, userId).then((r) => console.log(r));
    } else {
      let currentTime = new Date().toLocaleString("en-GB");
      // notifies that someone has joined the room
      writeOnChatHistory(
        "<div class='alert alert-light text-center' role='alert'>" +
          " <p>" +
          currentTime +
          "</p> <i class='far fa-bell fa-lg'></i>" +
          " <b>" +
          userId +
          "</b>" +
          " joined room " +
          room +
          " at: " +
          currentTime
      );
      autoScroll();
    }
  });
  // called when a message is received
  chat.on("chat", (room, userId, chatText) => {
    buildChatEntry(
      "fas fa-user-ninja fa-lg",
      "message-box-l alert alert-secondary",
      userId,
      chatText
    );
    // store the chat messages in the indexeddb
    storeChatLog(room, userId, chatText, currentTime, null, false, false);
    // scroll automatically to the end
    autoScroll();
  });
  //called when moving between rooms
  chat.on("move", (room, userId, option) => {
    console.log("process move room");
    createForm(option);
  });
  //called a knowledge graph is sent.
  chat.on("kg", (row, kgColor, name) => {
    console.log("kg client called");
    // store the original format of JSON-LD of Knowledge Graph into IndexedDB
    storeChatLog(
      roomNo,
      name,
      row,
      new Date().toLocaleString("en-GB"),
      kgColor,
      false,
      true
    );
    writeKGResult(row, kgColor, name);
    // automatically go to the end
    autoScroll();
  });
};

/**
 * build chat writes the information and styles to chat history
 * @icon is the favicon to use
 * @locaiton is the styles used to orient in the chat history
 * @who is the user id of who sent the chat
 * @chatText is the text to write to the chat
 * */
const buildChatEntry = (icon, location, who, chatText) => {
  let currentTime = new Date().toLocaleString("en-GB");
  writeOnChatHistory(
    "<div class='" +
      location +
      "' role='alert'>" +
      " <p>" +
      currentTime +
      "</p> <i class=" +
      icon +
      "></i>" +
      " <b>" +
      who +
      ":</b> " +
      chatText +
      "</div>"
  );
};

/**
 * create a form to input into chat history, stores room number and name,
 * user_id used from current session. call to moveNewRoom when link clicked
 *@imageUrl is the path to the image
 * */
const createForm = (imageUrl) => {
  // Create a form dynamically
  let form = document.createElement("form");
  // form.setAttribute("method", "post");
  form.setAttribute("onsubmit", "return false;");
  form.setAttribute("class", "alert alert-light text-center");

  //create input for image
  let img = document.createElement("input");
  img.setAttribute("type", "hidden");
  img.setAttribute("name", "image");
  img.setAttribute("id", imageName(imageUrl));
  img.setAttribute("value", imageUrl);

  // create time and icon
  let currentTime = new Date().toLocaleString("en-GB");
  let p = document.createElement("p");
  p.innerHTML = currentTime;
  let i = document.createElement("i");
  i.setAttribute("class", "fas fa-directions fa-lg text-primary");

  // create a submit link
  let a = document.createElement("a");
  a.setAttribute("id", "OpenRoom");
  a.setAttribute("onclick", "moveNewRoom(" + imageName(imageUrl) + ");");
  a.setAttribute("class", "moveRoom text-decoration-none pl-1 alias");
  a.textContent = "Move to another image: " + imageName(imageUrl);

  // Append the Room details to the form
  // form.appendChild(roomNo);

  // Append the img path to the form
  form.appendChild(img);

  // Append the time and icon
  form.appendChild(p);
  form.appendChild(i);

  // Append the submit button to the form
  form.appendChild(a);

  let history = document.getElementById("history");
  history.appendChild(form);

  // store the transition message in the indexeddb
  storeChatLog(room, userId, form.outerHTML, currentTime, null, true, false);
  // scroll automatically to the end
  autoScroll();
};

/**
 * ------------------------------------------------------------------------------------
 * code for the knowledge graph
 * ----------------------------------------------------------------------------------
 * */

/**
 * it inits the widget by selecting the type from the field myType
 * and it displays the Google Graph widget
 * it also hides the form to get the type
 */
function widgetInit() {
  let type = document.getElementById("myType").value;
  if (type) {
    let config = {
      limit: 5,
      languages: ["en"],
      types: [type],
      maxDescChars: 100,
      selectHandler: selectResult,
    };
    KGSearchWidget(apiKey, document.getElementById("myKGInput"), config);
    let myKGInput = document.getElementById("myKGInput");
    myKGInput.placeholder = "Search " + type.toLowerCase() + " as you type";
    myKGInput.value = "";
  }
}

/**
 * callback called when an element in the widget is selected
 * @param event the Google Graph widget event {@link https://developers.google.com/knowledge-graph/how-tos/search-widget}
 */
function selectResult(event) {
  // the search result
  let row = event.row;
  let kgColor = document.getElementById("kgOutput").getAttribute("value");
  // broadcast result to room with users color
  if (window.navigator.onLine) {
    chat.emit("kg", roomNo, row, kgColor, name);
  }
  // store the original format of JSON-LD of Knowledge Graph into IndexedDB
  storeChatLog(
    roomNo,
    name,
    row,
    new Date().toLocaleString("en-GB"),
    kgColor,
    false,
    true
  );
  // render the selected KG result
  writeKGResult(row, kgColor, name);
  // close modal after one result is selected
  $("#kgModal").modal("hide");
  // automatically go to the end
  autoScroll();
}

/**
 * function called to render the KG result (by appending the result to the chat history)
 * @param jsonld one row of the search results in JSON-LD format
 * @param kgColor current canvas color
 * @param name current user's name
 */
function writeKGResult(jsonld, kgColor, name) {
  // create outer div
  let outerDiv = document.createElement("div");
  let resultPanel = document.createElement("div");
  resultPanel.setAttribute("class", "resultPanel");

  // crate p element for result name
  let resultName = document.createElement("p");
  resultName.setAttribute("class", "text-center font-weight-bold");
  resultName.innerText = "Title: " + jsonld.name;

  // crate p element for result id
  let resultId = document.createElement("p");
  resultId.innerText = "ID: " + jsonld.id;

  // crate div element for result description
  let resultDescription = document.createElement("div");
  resultDescription.innerText = jsonld.rc;

  // crate url
  let urlDiv = document.createElement("div");
  let resultUrl = document.createElement("a");
  resultUrl.setAttribute("target", "_blank");
  resultUrl.setAttribute("href", jsonld.qc);
  resultUrl.innerText = "Link to Webpage";

  // append all elements
  urlDiv.appendChild(resultUrl);
  resultPanel.appendChild(resultName);
  resultPanel.appendChild(resultId);
  resultPanel.appendChild(resultDescription);
  resultPanel.appendChild(urlDiv);
  outerDiv.appendChild(resultPanel);

  //  create modal for result
  createModal(outerDiv, jsonld.name, kgColor, name);
}

/**
 * createModal follows the writeKgResult method call.
 * It creates a hidden modal to serve as a compressed method to dislplay the knowledge graph information
 * @param kg is the knowledge graph layout
 * @param jsonld is the knowledge graph name
 * @param kgColor is the annotation color associated with the knowledge graph
 * @param name is the userId of the person who sent the knowledge graph
 * */
const createModal = (kg, jsonld, kgColor, name) => {
  //remove punctuation
  let kgNameNoPunc = jsonld.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g, "");
  //remove spaces
  let kgName = kgNameNoPunc.replace(/\s/g, "");
  let modalId = kgName.replace("'", "") + dynamicModalCount;
  let modalLabel = modalId + "Label";
  //default sections
  //  modal body
  let mod = document.createElement("div");
  mod.setAttribute("class", "modal bd-example-modal-sm");
  mod.setAttribute("id", modalId);
  // mod.setAttribute("tabindex", "1000");
  mod.setAttribute("role", "dialog");
  mod.setAttribute("aria-labelledby", modalLabel);
  mod.setAttribute("aria-hidden", "true");
  mod.setAttribute("data-backdrop", "static");

  //  modal dialog
  let modDia = document.createElement("div");
  modDia.setAttribute("class", "modal-dialog modal-dialog-centered modal-sm");
  modDia.setAttribute("role", "document");

  //  modal content
  let modCon = document.createElement("div");
  modCon.setAttribute("class", "modal-content");

  //add content in these sections
  //  modal Header
  let modHead = document.createElement("div");
  modHead.setAttribute("class", "modal-header");
  //modal title
  let title = document.createElement("h6");
  title.setAttribute("class", "modal-title");
  title.innerText = "Knowledge graph from " + name;
  //modal close
  let headClose = document.createElement("button");
  headClose.setAttribute("type", "button");
  headClose.setAttribute("class", "close");
  headClose.setAttribute("data-dismiss", "modal");
  headClose.setAttribute("aria-label", "close");

  let x = document.createElement("span");
  x.setAttribute("aria-hidden", "true");
  x.innerText = "x";

  //build sections nested elements
  modHead.append(title);
  headClose.appendChild(x);
  modHead.appendChild(headClose);

  //  modal body
  let modBody = document.createElement("div");
  modBody.setAttribute("class", "modal-body");
  modBody.appendChild(kg);

  //  modal footer
  let modFoot = document.createElement("div");
  modFoot.setAttribute("class", "modal-footer");
  let footClose = document.createElement("button");
  footClose.setAttribute("type", "button");
  footClose.setAttribute("class", "btn btn-outline-secondary");
  footClose.setAttribute("data-dismiss", "modal");
  footClose.innerText = "Close";

  modFoot.appendChild(footClose);

  modCon.appendChild(modHead);
  modCon.appendChild(modBody);
  modCon.appendChild(modFoot);

  modDia.appendChild(modCon);
  //final compiled modal
  mod.appendChild(modDia);
  //modal button

  dynamicModalCount += 1;
  modalButton(mod, jsonld, modalId, kgColor);
};

/**
 * modalButton follows the createModal call. it is the final step that ties the created modal and button to the
 * chat history
 * @param modal is the created modal nodes from createModal
 * @param jsonld is the name associated with the knowledge graph
 * @param modalId is the css id for the modal
 * @param kgColor is the annotation color associated with the knowledge graph
 * */
const modalButton = (modal, jsonld, modalId, kgColor) => {
  let kgHistoryDiv = document.createElement("div");
  kgHistoryDiv.setAttribute("class", "text-center mb-2");
  let btn = document.createElement("a");
  btn.innerHTML = "KG: " + jsonld;
  btn.setAttribute("data-toggle", "modal");
  btn.setAttribute("class", "m-1 kg-button alias");
  btn.setAttribute("style", "background-color:" + kgColor);
  btn.setAttribute("data-target", "#" + modalId);

  let history = document.getElementById("history");
  let modalGroup = document.getElementById("hiddenModals");
  modalGroup.appendChild(modal);
  kgHistoryDiv.appendChild(btn);
  history.appendChild(kgHistoryDiv);
};

/**
 * called to allow user switching between canvas option modal and KG option modal
 * @param reverse true: from canvas option to KG option, false: from KG option to canvas option
 */
function openCanvasOption(reverse) {
  if (reverse) {
    $("#kgModal").modal("show");
    $("#canvasModal").modal("hide");
  } else {
    $("#kgModal").modal("hide");
    $("#canvasModal").modal("show");
  }
}

/**
 * scroll automatically after sending/receiving a new message
 */
function autoScroll() {
  let chatArea = document.getElementById("history");
  document.getElementById("history").scrollTop = chatArea.scrollHeight;
}

/**
 * called when the Send button is pressed. It gets the text to send from the interface
 * and sends the message via socket
 */
const sendChatText = () => {
  let chatText = document.getElementById("chat_input").value;
  let currentTime = new Date().toLocaleString("en-GB");

  buildChatEntry(
    "fas fa-user-secret fa-lg",
    "message-box-r alert alert-success",
    "me",
    chatText
  );
  storeChatLog(room, userId, chatText, currentTime, null, false, false);
  if (window.navigator.onLine) {
    chat.emit("chat", roomNo, name, chatText);
  }
};

/**
 * called when link to new room used. creates new sockets, clears chat in view and inits new canvas
 * */
function moveNewRoom(image) {
  let imagePath = image.value;
  //event to leave and disconnect connection with previous room
  if (window.navigator.onLine) {
    chat.emit("leave", roomNo);
    socket.emit("leave", roomNo);
  }
  //create no room number
  roomNo = savedRoom + "-" + image.id;

  //close listeners for canvas, re-initialised in initCanvas call
  if (window.navigator.onLine) {
    socket.off("draw");
    socket.off("clear");

    //connect to new socket
    chat.emit("create or join", roomNo, name);
    socket.emit("create or join", roomNo);
  }

  let offline = document.getElementById("offlineAlert");

  if (window.navigator.onLine) {
    offline.style.display = "none";
  } else {
    offline.style.display = "block";
  }

  const container = document.getElementById("history");
  removeAllChatChildNodes(container);

  // initCanvas(socket, imagePath, roomNo, name);
  initCanvasCached(imagePath);
}

/**
 * removes all created nodes in chat history. pass in parent argument history in chat_interface.ejs
 * */
const removeAllChatChildNodes = (parent) => {
  while (parent.firstChild) {
    parent.removeChild(parent.firstChild);
  }
};

/**
 * called as part of the chat interface. sends new room information via socket.io
 * */
const sendNewRoom = () => {
  console.log("send new room called");
  let imageUrl = document.getElementById("modal_image_url").value;
  let imageDirectUrl = document.getElementById("modal_image_direct_url").value;

  if (imageDirectUrl !== "") {
    createForm(imageDirectUrl);
    if (window.navigator.onLine) {
      chat.emit("move room", roomNo, name, imageDirectUrl);
    }
  } else {
    createForm(imageUrl);
    if (window.navigator.onLine) {
      chat.emit("move room", roomNo, name, imageUrl);
    }
  }
};

/**
 * function from initial code, writeOnChatHistory appends text to the nodes in chat interface
 * */
const writeOnChatHistory = (text) => {
  let history = document.getElementById("history");
  let paragraph = document.createElement("p");
  paragraph.innerHTML = text;
  history.appendChild(paragraph);
  document.getElementById("chat_input").value = "";
};

/**
 * used to connect to a room. It gets the user name and room number from the
 * interface
 */
const connectToRoom = () => {
  name = document.getElementById("name").value;
  let imageUrl = document.getElementById("image_url").value;
  let imageDirectUrl = document.getElementById("image_direct_url").value;
  savedRoom = document.getElementById("roomNo").value;

  let offline = document.getElementById("offlineAlert");
  let homeOffline = document.getElementById("homeOfflineAlert");
  let kg = document.getElementById("kgButton");

  if (window.navigator.onLine) {
    offline.style.display = "none";
  } else {
    kg.setAttribute("class", "dropdown-item disabled");
    kg.setAttribute("aria-disabled", "true");
    offline.style.display = "block";
    homeOffline.style.display = "none";
  }

  // form validation
  if (
    (imageUrl === "Select " + "image from list" && imageDirectUrl === "") ||
    savedRoom === ""
  ) {
    openNotification(
      "Please fill in all required information to join/start a meeting!"
    );
    return;
  }

  roomNo = document.getElementById("roomNo").value + "-" + imageName(imageUrl);
  if (!name) name = "Anonymous-" + Math.round(Math.random() * 10000);
  if (window.navigator.onLine) {
    chat.emit("create or join", roomNo, name);
    socket.emit("create or join", roomNo);
  }

  // If the user provides a direct link to an image, the direct image link is used in preference.
  if (imageDirectUrl !== "") {
    initCanvasCached(imageDirectUrl);
  } else {
    initCanvas(socket, imageUrl, roomNo, name);
  }
  listImages("modal_image_url");

  hideLoginInterface(roomNo, name);
};

/**
 * Given an image path, if the image is cached, it is loaded from the cache.
 *
 * @param image_url the image path
 */
function initCanvasCached(image_url) {
  // use the image that has been cached, if it exists
  retrieveCachedImage(image_url).then((r) => {
    if (r === undefined || r.image === undefined) {
      initCanvas(socket, image_url, roomNo, name);
    } else {
      initCanvas(socket, r.image, roomNo, name);
    }
  });
}

/**
 * called to set the height of chat history dynamically after the canvas is built.
 * */
function setDynamicChatHeight() {
  let topHeight =
    document.getElementById("chat-header").clientHeight +
    document.getElementById("chat-image").clientHeight;
  let botHeight = document.getElementById("input").clientHeight;
  document.getElementById("history").style.top = topHeight + "px";
  document.getElementById("history").style.bottom = botHeight + "px";
}

/**
 * it appends the given html text to the history div
 * this is to be called when the socket receives the chat message (socket.on ('message'...)
 * @param text: the text to append
 */
function writeOnHistory(text) {
  if (text === "") return;
  let history = document.getElementById("history");
  let paragraph = document.createElement("p");
  paragraph.innerHTML = text;
  history.appendChild(paragraph);
  // scroll to the last element
  history.scrollTop = history.scrollHeight;
  document.getElementById("chat_input").value = "";
}

/**
 * called to open a notification modal with provided message
 * @param text
 */
function openNotification(text) {
  // set error message
  document.getElementById("notification").innerHTML = text;
  // open notification modal
  $("#notificationModal").modal("show");
}

/**
 * it hides the initial form and shows the chat
 * @param room the selected room
 * @param userId the user name
 */
function hideLoginInterface(room, userId) {
  document.getElementById("home").style.display = "none";
  document.getElementById("chat_interface").style.display = "block";
  document.getElementById("home_header").style.display = "none";
  document.getElementById("container").style.display = "none";
  document.getElementById("image_upload").style.display = "none";
  document.getElementById("who_you_are").innerHTML = userId;
  document.getElementById("in_room").innerHTML = " " + room;
}

/**
 * removes the prepended path from the image details
 * @param image is the full image path
 * */
const imageName = (image) => {
  let name = image
    .substring(image.lastIndexOf("/") + 1)
    .split(".")
    .shift();

  return name[0].toUpperCase() + name.slice(1);
};

/************************** upload image **************************/

/**
 * main page image upload handle function
 */
function uploadImage() {
  let image_base64_head = "data:image/";
  let author = document.getElementById("image_author").value;
  let description = document.getElementById("image_description").value;
  let title = document.getElementById("image_title").value;

  // form validation
  if (
    uploadImgBase64Data == null ||
    title === "" ||
    description === "" ||
    author === ""
  ) {
    openNotification("Please enter all required information before uploading!");
    return;
  }

  if (uploadImgBase64Data.includes(image_base64_head)) {
    const data = {};

    // pass form data(uploaded image's base64 string) to backend.
    data.base64 = uploadImgBase64Data;
    data.author = author;
    data.description = description;
    data.title = title;

    // call ajax query
    uploadImgAjaxQuery("/api/image", data);
  } else {
    openNotification("Please upload image only!");
  }
}

/**
 * main page image upload ajax query
 */
function uploadImgAjaxQuery(url, data) {
  $.ajax({
    url: url,
    data: JSON.stringify(data),
    contentType: "application/json",
    dataType: "json",
    type: "POST",
    success: function (res) {
      // main page upload validation
      if (res.fileName === "" || res.filePath === "") {
        res.sendStatus(404);
        res.end();
      } else {
        let image_name = JSON.stringify(res.fileName);
        openNotification(
          "Image " +
            image_name +
            " uploaded successfully! You can select your image from list to initialise a meeting!"
        );
        // get the latest version of image list
        $("#image_url").empty();
        listImages("image_url");
      }
    },
    error: function (err) {
      console.log(err.message);
    },
  });
}

/**
 * set parameter uploadImgBase64Data = image base64 string
 * pass it to backend.
 */
function getBase64String() {
  const file = document.querySelector("input[id=customFile]").files[0];
  const reader = new FileReader();

  reader.addEventListener(
    "load",
    function () {
      // reader.result is the base64 string of uploaded image.
      uploadImgBase64Data = reader.result;
    },
    false
  );

  if (file) {
    reader.readAsDataURL(file);
  }
}

/************************** snapshot **************************/

/**
 * webRTC interface
 */
function captureImage() {
  let video = document.querySelector("video");
  let canvas = (window.canvas = document.querySelector("canvas"));
  canvas.width = 320;
  canvas.height = 240;
  canvas.width = video.videoWidth;
  canvas.height = video.videoHeight;
  canvas.getContext("2d").drawImage(video, 0, 0, canvas.width, canvas.height);

  let constraints = {
    audio: false,
    video: { width: 320, height: 240 },
  };

  navigator.mediaDevices
    .getUserMedia(constraints)
    .then(function handleSuccess(stream) {
      console.log("called statement2!!!");

      window.stream = stream;
      video.srcObject = stream;
    })
    .catch(function handleError(error) {
      console.log("navigator.getUserMedia error: ", error);
    });
}

/**
 * canvas.toDataURL base64 string is too large so it
 * can't be sent to backend directly, this function
 * save base64 string to a hidden input text field.
 */
function saveImage() {
  // get base64
  let canvas = document.getElementById("my_canvas");
  let imgBase64 = canvas.toDataURL("image/jpeg");

  // fill hidden form to get base64 data
  let base64_field = document.getElementById("base64");
  base64_field.value = imgBase64;

  // snapshot upload validation: the length of blank canvas
  // base64 string is 783
  if (base64_field.value.length === 783) {
    snapshotValidation = false;
  } else {
    snapshotValidation = true;
    openNotification("snapshot uploaded! Select snapshot from list!");
  }
}

/**
 * snapshot handle function
 */
function snapshotSubmit() {
  let formArray = $("form").serializeArray();
  let data = {};

  for (let index in formArray) {
    data[formArray[index].name] = formArray[index].value;
  }

  // snapshot upload validation
  if (snapshotValidation === false) {
    openNotification("Please take a picture first then upload!");
  } else {
    snapshotAjaxQuery("/api/image", data);
  }
}

/**
 * function snapshotAjaxQuery and snapshotSubmit receive base64 data
 * from the hidden input text field and send back to backend.
 */
function snapshotAjaxQuery(url, data) {
  $.ajax({
    url: url,
    data: JSON.stringify(data),
    contentType: "application/json",
    dataType: "json",
    type: "POST",
    success: function (dataR) {
      // snapshot upload validation
      if (dataR.fileName === "" && dataR.filePath === "") {
      } else {
        let snapshot_path = JSON.stringify(dataR.filePath);
        let snapshot_name = JSON.stringify(dataR.fileName);

        // append option for select box so that user can select uploaded image
        // to display in chat interface
        $("#image_url").append(
          '<option id="upload" value=img_path>img_name</option>'
        );
        document.getElementById("upload").text = snapshot_name.slice(1, -1);
        document.getElementById("upload").value = snapshot_path.slice(1, -1);

        // get the latest version of image list
        $("#image_url").empty();
        listImages("image_url");
      }
    },
    error: function (response) {},
  });
}

function MongoOnSubmit() {
  let data = {};
  let title = document.getElementById("image_title").value;
  let description = document.getElementById("image_description").value;
  let author = document.getElementById("image_author").value;

  data.image_title = title;
  data.image_description = description;
  data.image_author = author;

  sendMongoDBAjaxQuery("/api/image/save", data);
}

function sendMongoDBAjaxQuery(url, data) {
  $.ajax({
    url: url,
    data: data,
    dataType: "json",
    type: "POST",
    success: function (dataR) {},
    error: function (xhr, status, error) {},
  });
}
