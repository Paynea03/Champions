/**
 * this file contains the functions to control the drawing on the canvas
 */
let room;
let userId;
let red = 255;
let green = 0;
let blue = 0;
let color = "rgb(" + red + ", " + green + ", " + blue + ")",
  thickness = 4;
let canvas;
let counter = 0;

/**
 * setCanvasInput call is used to set the annotation colors and line widths inside a chat.
 * */
const setCanvasInput = () => {
  //set width
  let slider = document.getElementById("lineWidth");
  let lineOutput = document.getElementById("lineOutput");

  // Update the current slider value (each time you drag the slider handle)
  slider.oninput = function () {
    lineOutput.innerHTML = this.value;
    thickness = this.value;
  };

  //get elements from modal for color, box and output as text and display of current color
  let canvasColorPicker = document.getElementById("canvasColorPicker");
  let canvasBox = document.getElementById("canvasBox");
  let canvasOutput = document.getElementById("canvasOutput");
  let kgOutput = document.getElementById("kgOutput");

  canvasBox.style.borderColor = canvasColorPicker.value;

  //event listeners to detect new color change in color picker
  canvasColorPicker.addEventListener(
    "input",
    function (event) {
      console.log("input event");
      canvasBox.style.borderColor = event.target.value;
    },
    false
  );

  canvasColorPicker.addEventListener(
    "change",
    function (event) {
      console.log("change event");
      canvasOutput.innerText = "Color set to:" + canvasColorPicker.value;
      kgOutput.innerText = "Color set to:" + canvasColorPicker.value;
      kgOutput.setAttribute("value", canvasColorPicker.value);
      color = canvasColorPicker.value;
    },
    false
  );
};

/**
 * it inits the image canvas to draw on. It sets up the events to respond to (click, mouse on, etc.)
 * it is also the place where the data should be sent via socket.io
 * @param sckt the open socket to register events on
 * @param imageUrl the image url to downloads
 */
function initCanvas(sckt, imageUrl, roomNo, name) {
  setCanvasInput();
  socket = sckt;
  room = roomNo;
  userId = name;
  let flag = false,
    prevX,
    prevY,
    currX,
    currY = 0;
  canvas = $("#canvas");
  let cvx = document.getElementById("canvas");
  let img = document.getElementById("image");
  let ctx = cvx.getContext("2d");
  img.src = imageUrl;

  // event on the canvas when the mouse is on it
  canvas.on("mousemove mousedown mouseup mouseout", function (e) {
    prevX = currX;
    prevY = currY;
    currX = e.clientX - canvas.position().left;
    currY = e.clientY - canvas.position().top;

    if (e.type === "mousedown") {
      flag = true;
    }
    if (e.type === "mouseup" || e.type === "mouseout") {
      flag = false;
    }
    // if the flag is up, the movement of the mouse draws on the canvas
    if (e.type === "mousemove") {
      let canvasAnnot = {
        width: canvas.width,
        height: canvas.height,
        x1: prevX,
        y1: prevY,
        x2: currX,
        y2: currY,
        col: color,
        thick: thickness,
      };
      if (flag) {
        drawOnCanvas(ctx, canvasAnnot);
        if (window.navigator.onLine) {
          socket.emit("draw", room, userId, canvasAnnot);
        }
      }
    }
  });

  // click on the button to clear the canvas (on both side)
  $(".canvas-clear").on("click", function (e) {
    // remove annotations related to this room from the local IndexedDB
    clearAnnotation(room).then((r) => console.log(r));

    let c_width = canvas.width;
    let c_height = canvas.height;
    ctx.clearRect(0, 0, c_width, c_height);
    // draw the image onto the canvas
    buildCanvas();
    if (window.navigator.onLine) {
      socket.emit("clear canvas", room, userId);
    }
  });
  if (window.navigator.onLine) {
    // socket listener setup
    socket.on("draw", (canvasAnnot, userId) => {
      let ctx = canvas[0].getContext("2d");
      drawOnCanvas(ctx, canvasAnnot, userId);
    });

    socket.on("clear", () => {
      // remove annotations related to this room from the local IndexedDB
      clearAnnotation(room).then((r) => console.log(r));

      let c_width = canvas.width;
      let c_height = canvas.height;
      ctx.clearRect(0, 0, c_width, c_height);
      // draw the image onto the canvas
      buildCanvas();
    });
  }
  // this is called when the src of the image is loaded
  function buildCanvas() {
    img.style.display = "block";
    // resize the canvas
    let ratioX = 1;
    let ratioY = 1;
    // if the screen is smaller than the img size we have to reduce the image to fit
    if (img.clientWidth > window.innerWidth)
      ratioX = window.innerWidth / img.clientWidth;
    if (img.clientHeight > window.innerHeight)
      ratioY = img.clientHeight / window.innerHeight;
    let ratio = Math.min(ratioX, ratioY);
    // resize the canvas to fit the screen and the image
    cvx.width = canvas.width = img.clientWidth * ratio;
    cvx.height = canvas.height = img.clientHeight * ratio;
    // draw the image onto the canvas
    drawImageScaled(img, cvx, ctx);
    // hide the image element as it is not needed
    img.style.display = "none";
    // set the height of chat history area
    setDynamicChatHeight();
  }

  // this is an async operation as it may take time
  img.addEventListener(
    "load",
    () => {
      // it takes time before the image size is computed and made available
      // here we wait until the height is set, then we resize the canvas based on the size of the image
      let poll = setInterval(function () {
        if (img.naturalHeight) {
          clearInterval(poll);
          buildCanvas();
          // load all previous annotations after building the canvas
          retrieveAnnotation(ctx, room, userId).then((r) => console.log(r));
          // cache the image from a remote url
          // same image will be cached only once
          let base64Img = document
            .getElementById("canvas")
            .toDataURL("image/png");
          cacheRemoteImage(base64Img, userId, img.src).catch((error) => {
            console.log(
              "[IndexedDB] Caching aborted, this image has already been cached: " +
                img.src
            );
          });
        }
      }, 10);
    },
    { once: true }
  );
}

/**
 * called when it is required to draw the image on the canvas. We have resized the canvas to the same image size
 * so ti is simpler to draw later
 * @param img
 * @param canvas
 * @param ctx
 */
function drawImageScaled(img, canvas, ctx) {
  // get the scale
  let scale = Math.min(canvas.width / img.width, canvas.height / img.height);
  // get the top left position of the image
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  let x = canvas.width / 2 - (img.width / 2) * scale;
  let y = canvas.height / 2 - (img.height / 2) * scale;
  ctx.drawImage(img, x, y, img.width * scale, img.height * scale);
}

/**
 * this is called when we want to display what we (or any other connected via socket.io) draws on the canvas
 * note that as the remote provider can have a different canvas size (e.g. their browser window is larger)
 * we have to know what their canvas size is so to map the coordinates
 * @param ctx the canvas context
 * @param canvasWidth the originating canvas width
 * @param canvasHeight the originating canvas height
 * @param prevX the starting X coordinate
 * @param prevY the starting Y coordinate
 * @param currX the ending X coordinate
 * @param currY the ending Y coordinate
 * @param color of the line
 * @param thickness of the line
 */
function drawOnCanvas(ctx, canvasAnnot, author) {
  //get the ration between the current canvas and the one it has been used to draw on the other computer
  let ratioX = canvas.width / canvasAnnot.width;
  let ratioY = canvas.height / canvasAnnot.height;
  // update the value of the points to draw
  canvasAnnot.x1 *= ratioX;
  canvasAnnot.y1 *= ratioY;
  canvasAnnot.x2 *= ratioX;
  canvasAnnot.y2 *= ratioY;
  ctx.beginPath();
  ctx.moveTo(canvasAnnot.x1, canvasAnnot.y1);
  ctx.lineTo(canvasAnnot.x2, canvasAnnot.y2);
  ctx.strokeStyle = canvasAnnot.col;
  ctx.lineWidth = canvasAnnot.thick;
  ctx.stroke();
  ctx.closePath();
  if (window.navigator.onLine) {
    // store room id, user id and (scaled) annotation object to the local IndexedDB, only if online
    storeAnnotation(room, author || userId, canvasAnnot);
  }
}
