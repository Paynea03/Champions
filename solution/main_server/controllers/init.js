const mongoose = require('mongoose');
const Image = require('../../mongo_server/models/image');

/**
 * Initialize mongoDB image collection
 */
exports.init = function () {
  let image = new Image({
    title: 'init title',
    description: 'init description',
    author: 'init author',
    date: 0,
    img: 'init img'
  });

  image.save(function (err, results) {
    console.log('initialized data');
  });
}
