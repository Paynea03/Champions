const imageData = require('../../mongo_server/models/image');
const fetch = require('node-fetch');

exports.chatInterfaceImageData = function (req, res) {
  let userData = req.body;
  let getTime = Date();
  let imgPath = "./private/images/" + userData.image_title;
  fetch('http://localhost:3005/api/mongo/image', {
    method: 'post',
    body: JSON.stringify({
      title: userData.image_title,
      description: userData.image_description,
      author: userData.image_author,
      date: getTime,
      img: imgPath
    }),
    headers: {'Content-Type': 'application/json'},
  }).then(res.sendStatus(200))
    .catch(err => {
      console.log(err.message)
      res.sendStatus(400);
      res.end();
    });
};
