const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");

//get image list code--------------------------------------------------------
// const imageFolder = path.join(__dirname, "private/images");
//
// const fs = require("fs");
//
// fs.readdir(imageFolder, (err, files) => {
//   files.forEach((file) => {
//     console.log(file);
//   });
// });
//--------------------------------------------------------

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");

const app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(logger("dev"));
// increase the limit to accept more data
app.use(express.json({ limit: "5mb" }));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);

app.use(express.static(path.join(__dirname, "private")));
app.use("/images", express.static("images"));

// swagger setup
const swaggerUI = require("swagger-ui-express");
const openApiDocumentation = require("./swagger/openapi.json");
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(openApiDocumentation));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handlers
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
