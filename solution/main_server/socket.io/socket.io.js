/**
 * Initialise a new socket based on room id,
 * allow users to send messages just in that particular room
 *
 * @param io the io instance
 */
exports.init = (io) => {
  // the chat namespace
  const chat = io.of("/chat").on("connection", (socket) => {
    try {
      socket.on("create or join", (room, userId) => {
        console.log("Joined Chat Room: " + room);
        socket.join(room);
        chat.to(room).emit("joined", room, userId);
      });

      //socket for chat history
      socket.on("chat", (room, userId, chatText) => {
        socket.broadcast.to(room).emit("chat", room, userId, chatText);
      });

      //socket for finding move between roome events
      socket.on("move room", (room, userId, option) => {
        socket.broadcast.to(room).emit("move", room, userId, option);
      });

      //disconnect event
      socket.on("disconnect", () => {
        console.log("chat disconnected");
      });

      //socket for sending knowledge graph information
      socket.on("kg", (room, row, kgColor, name) => {
        console.log("broadcast called");
        socket.broadcast.to(room).emit("kg", row, kgColor, name);
      });

      //socket for disconnecting previous room when moving between rooms
      socket.on("leave", (room) => {
        console.log("Left Chat Room: " + room);
        socket.leave(room);
      });
    } catch (e) {
      console.log("socket error");
      console.log(e);
    }
  });

  /**
   * namespace for the annotations on canvas.
   * */
  //The socket for sending annotations
  const draw = io.of("/draw").on("connection", (socket) => {
    try {
      socket.on("create or join", (room) => {
        console.log("Joined draw Room: " + room);
        socket.join(room);
      });

      //socket for handling annotations on canvas
      socket.on("draw", (room, userId, canvasAnnot) => {
        socket.broadcast.to(room).emit("draw", canvasAnnot, userId);
      });

      //socket for handling clearing annotations on canvas
      socket.on("clear canvas", (room, userId) => {
        socket.broadcast.to(room).emit("clear");
      });

      socket.on("disconnect", () => {
        console.log("draw socket disconnected");
      });
      //socket for disconnecting previous room when moving between rooms
      socket.on("leave", (room) => {
        console.log("Left Draw Room: " + room);
        socket.leave(room);
      });
    } catch (e) {
      console.log("socket error draw");

      console.log(e);
    }
  });
};
