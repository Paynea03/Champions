const express = require("express");
const fs = require("fs");
const path = require("path");
const router = express.Router();
const image = require("../controllers/image");
const initDB = require("../controllers/init");
const imageData = require("../../mongo_server/models/image");
initDB.init();

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("layouts/layout", {
    title: "Team Champions secret messaging project",
    template: "index",
  });
});

router.get("/api/image", function (req, res, next) {
  //gets the path to the image folder.
  const imageFolder = path.join(__dirname, "../private/images");

  fs.readdir(imageFolder, (err, files) => {
    if (files.length === 0) {
      // 404: no files found
      res.sendStatus(404);
    }

    res.setHeader("Content-Type", "application/json");
    res.json(files);
  });
});

/* main page image upload POST. */
router.post("/api/image", function (req, res) {
  let dir = path.join(__dirname, "/../private");

  // image upload validation
  if (req.body.base64 === "") {
    // 400: bad request
    res.sendStatus(400);
  } else {
    // buf is the image base64 string data
    let imageData = req.body;
    let imageBlob = imageData.base64.replace(/^data:image\/\w+;base64,/, "");
    let buf = new Buffer(imageBlob, "base64");

    // get the path/name of uploaded image
    let timestamp = new Date().getTime();
    let imageName = imageData.title + timestamp + ".png";
    let imagePath = dir + "/images/" + imageName;

    // write image file
    fs.writeFile(imagePath, buf, "base64", (err) => {
      if (err) {
        // 500: server side error
        res.sendStatus(500);
      } else {
        console.log("Image uploaded!");
      }
    });

    // response body
    res.send({
      filePath: imagePath,
      fileName: imageName,
    });
  }
});

/* mongoDB send image data POST. */
router.post("/api/image/save", image.chatInterfaceImageData);

module.exports = router;
