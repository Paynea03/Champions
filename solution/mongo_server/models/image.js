const mongoose = require('mongoose');

const Schema = mongoose.Schema;

/**
 * set up the mongoDB chat upload image schema
 */

const Image = new Schema(
  {
    title: {type: String, required: true, max: 100},
    description: {type: String, required: true, max: 100},
    author: {type: String, required: true, max: 100},
    date: {type: String, required: true, max: 100},
    img: {type: String, required: true, max: 100}
  }
);

module.exports = mongoose.model('Image', Image);