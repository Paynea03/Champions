/**
 * File used to establish mongoDB database connection
 * By default, its running on port 27017
 */

'use strict'
const mongoose = require('mongoose');
const MONGO_DB_NAME = 'champions';
const MONGO_DB_URL = `mongodb://localhost:27017/${MONGO_DB_NAME}`

/**
 * Establish mongoDB database connection
 */
mongoose.Promise = global.Promise;

mongoose.connect(MONGO_DB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  checkServerIdentity: false,
}).then(() => {
  console.log('MongoDB connection established successfully!');
}).catch(err => 'Error in MongoDB connection: ' + err.message);
