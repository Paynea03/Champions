const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const imageData = require('../models/image');
router.use(bodyParser.json());

/* GET home page. */
router.post('/api/mongo/image', function (req, res, next) {

  let imgRecords = new imageData({
    title: req.body.title,
    description: req.body.description,
    author: req.body.author,
    date: req.body.date,
    img: req.body.img
  });

  console.log(req.body);

  imgRecords.save(function (err) {
    if (err) {
      console.log(err.message)
      res.sendStatus(500);
      res.end();
    } else {
      console.log('MongoDB received data!');
      // response body
      res.send({
        title: imgRecords.title,
        description: imgRecords.description,
        author: imgRecords.author,
        date: imgRecords.date,
        img: imgRecords.img
      });
    }
  });

});

module.exports = router;
